package com.plightlab.milszym.plightcompilator.compilator;

import android.app.Activity;
import android.content.Context;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.plightlab.milszym.plightcompilator.*;
import com.plightlab.milszym.plightcompilator.listviewitems.CustomAdapterRegisters;

/**
 * Created by Szczypiorek on 01.05.2017.
 */

public class Compilator {

    private String[] commands = {"ADD", "SUB", "MOV"};
    private String[] registerNames = {"AX", "BX", "CX", "DX"};
    private ListView registers;
    private Context context;

    public Compilator(Context context) {
        this.context=context;
        registers = (ListView)((Activity)context).findViewById(R.id.registersListView);
    }

    //analyzing command and choosing next method that will proceed our command
    public void analyzeCommand(String commandString) {

        String[] words = commandString.split(" ");
        ArrayList<String> wordsList = new ArrayList<String>();
        wordsList.addAll(Arrays.asList(words));

        if (wordsList.get(0).equals(commands[0])) {
            add(wordsList.get(1), wordsList.get(2));
        } else if (wordsList.get(0).equals(commands[1])) {
            sub(wordsList.get(1), wordsList.get(2));
        } else if (wordsList.get(0).equals(commands[2])) {
            mov(wordsList.get(1), wordsList.get(2));
        }

    }

    //procedure of adding, the command template is looking like this: ADD VALUE, REGISTER
    private void add(String arg0, String arg1) {
        //TODO: create procedure that will execute adding command (change Views etc.)
        if(arg0.contains("AX")) {
            changeViewValues("AX", "13", "abcde000", "kakaroko");
        }
    }

    //procedure of substracting, the command template is looking like this: SUB VALUE, REGISTER
    private void sub(String arg0, String arg1) {
        //TODO: create procedure that will execute substracting command (change Views etc.)
    }

    //procedure of moving, the command template is looking like this: SUB FROMREGISTER, TOREGISTER
    private void mov(String arg0, String arg1) {
        //TODO: create procedure that will execute moving command (change Views etc.)

    }

    // This method is replacing ListView with data that you put into it
    //example of use: changeViewValues("AX", "00010101", "01111000") will change the first item of ListView to : AX 00010101 01111000
    private void changeViewValues(String registerName, String integerValue, String oldBits, String newBits) {

        if (registerName.equals(registerNames[0])) {
            CustomAdapterRegisters csr = (CustomAdapterRegisters)(registers.getAdapter());
            csr.update(0, integerValue, oldBits, newBits);
            registers.setAdapter(csr);
        } else if (registerName.equals(commands[1])) {
            CustomAdapterRegisters csr = (CustomAdapterRegisters)(registers.getAdapter());
            csr.update(1, integerValue, oldBits, newBits);
            registers.setAdapter(csr);
        } else if (registerName.equals(commands[2])) {
            CustomAdapterRegisters csr = (CustomAdapterRegisters)(registers.getAdapter());
            csr.update(2, integerValue, oldBits, newBits);
            registers.setAdapter(csr);
        } else if (registerName.equals(commands[3])) {
            CustomAdapterRegisters csr = (CustomAdapterRegisters)(registers.getAdapter());
            csr.update(3, integerValue, oldBits, newBits);
            registers.setAdapter(csr);
        }

    }

    //this method is returning String value from ListView items
    //example of use: getValuesFromListView("AX", 2) will return integer representation of all bits from register AX
    // ( if whichPart equals 3 then it will return old bits, whichPart equals 4 will return new bits and of course whichPart==1 will return the name of register)
    private String getValuesFromListView(String registerName, int whichPart){

        if (registerName.equals(registerNames[0])) {
            CustomAdapterRegisters csr = (CustomAdapterRegisters)(registers.getAdapter());
            csr.getValues(0, whichPart);
        } else if (registerName.equals(commands[1])) {
            CustomAdapterRegisters csr = (CustomAdapterRegisters)(registers.getAdapter());
            csr.getValues(1, whichPart);
        } else if (registerName.equals(commands[2])) {
            CustomAdapterRegisters csr = (CustomAdapterRegisters)(registers.getAdapter());
            csr.getValues(2, whichPart);
        } else if (registerName.equals(commands[3])) {
            CustomAdapterRegisters csr = (CustomAdapterRegisters)(registers.getAdapter());
            csr.getValues(3, whichPart);
        }
        return null;

    }


}
