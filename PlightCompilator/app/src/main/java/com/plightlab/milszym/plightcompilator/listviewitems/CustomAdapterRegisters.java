package com.plightlab.milszym.plightcompilator.listviewitems;

import com.plightlab.milszym.plightcompilator.*;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Szczypiorek on 01.05.2017.
 */

public class CustomAdapterRegisters extends ArrayAdapter<CustomObjectRegisters> {

    private TextView tvRegister, tvOldBits, tvNewBits, tvInteger;
    private ArrayList<CustomObjectRegisters> objects;
    private ListView registers;
    private Context context;

    //<--------------- Constructing basic values of ListView ------------------------>
    public CustomAdapterRegisters(Context context, ArrayList<CustomObjectRegisters> objects, ListView ls) {
        super(context, 0, objects);
        this.objects = objects;
        this.context = context;

        registers = ls;
        registers.setAdapter(this);
        //adding basic values to ArrayList
        objects = new ArrayList<CustomObjectRegisters>();
        objects.add(new CustomObjectRegisters((context).getString(R.string.registerA), (context).getString(R.string.tmpRegisterValue), (context).getString(R.string.tmpRegisterValue),"1"));
        objects.add(new CustomObjectRegisters((context).getString(R.string.registerB), (context).getString(R.string.tmpRegisterValue), (context).getString(R.string.tmpRegisterValue), "2"));
        objects.add(new CustomObjectRegisters((context).getString(R.string.registerC), (context).getString(R.string.tmpRegisterValue), (context).getString(R.string.tmpRegisterValue), "3"));
        objects.add(new CustomObjectRegisters((context).getString(R.string.registerD), (context).getString(R.string.tmpRegisterValue), (context).getString(R.string.tmpRegisterValue), "4"));
        //inserting values to adapter
        addAll(objects);

        setNotifyOnChange(true);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        CustomObjectRegisters user = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.register_item, parent, false);
        }
        // Lookup view for data population
        tvRegister = (TextView) convertView.findViewById(R.id.registerName);
        tvInteger = (TextView) convertView.findViewById(R.id.integerInItem);
        tvOldBits = (TextView) convertView.findViewById(R.id.oldBitsPart);
        tvNewBits = (TextView) convertView.findViewById(R.id.newBitsPart);
        // Populate the data into the template view using the data object
        tvRegister.setText(user.getProp1());
        tvOldBits.setText(user.getProp2());
        tvNewBits.setText(user.getProp3());
        tvInteger.setText(user.getProp4());

        //adding fonts
        Typeface face = Typeface.createFromAsset(((Activity) context).getAssets(), "forest.ttf");
        tvRegister.setTypeface(face);
        tvOldBits.setTypeface(face);
        tvNewBits.setTypeface(face);
        tvInteger.setTypeface(face);

        // Return the completed view to render on screen
        return convertView;
    }

    //method that will update current ListView
    public void update(int position, String integer, String oldBits, String newBits) {
        objects.get(position).setProp2(oldBits);
        objects.get(position).setProp3(newBits);
        objects.get(position).setProp4(integer);
    }

    //method that will return the exact value of ListView items
    //e.g. getValues(1, 3) will return new bits from register BX
    public String getValues(int position, int whichPart) {
        switch (whichPart) {
            case 1: {
                return objects.get(position).getProp1();
            }
            case 2: {
                return objects.get(position).getProp4();
            }
            case 3: {
                return objects.get(position).getProp2();
            }
            case 4: {
                return objects.get(position).getProp3();
            }
            default: {
                return objects.get(position).getProp1();
            }
        }
    }

}



