package com.plightlab.milszym.plightcompilator;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    // <---------------- Views ------------------>
    private TextView welcomeMessageTV;
    private ConstraintLayout cl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // <---------------- Initalizers ------------------>
        welcomeMessageTV = (TextView)findViewById(R.id.welcomeMessageTV);
        Typeface face = Typeface.createFromAsset(getAssets(), "black_napkins.ttf");
        welcomeMessageTV.setTypeface(face);

        animateRotationAndBackingOff(welcomeMessageTV);

        // setting onClick (anywhere) going to next activity
        cl = (ConstraintLayout)findViewById(R.id.cl);
        cl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startCompilatorActivity();
            }
        });
    }

    private void animateRotationAndBackingOff(View v){
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.come_in);
        anim.setFillAfter(true);
        v.setAnimation(anim);
    }

    private void startCompilatorActivity(){
        Intent intent = new Intent(this, CompilatorActivity.class);
        startActivity(intent);
    }

}
