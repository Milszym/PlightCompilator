package com.plightlab.milszym.plightcompilator;

import android.graphics.Color;
import android.graphics.Typeface;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.MultiAutoCompleteTextView;

import com.plightlab.milszym.plightcompilator.compilator.Compilator;
import com.plightlab.milszym.plightcompilator.listviewitems.CustomAdapterRegisters;
import com.plightlab.milszym.plightcompilator.listviewitems.CustomObjectRegisters;
import com.plightlab.milszym.plightcompilator.tokenizer.SpaceTokenizer;

import java.util.ArrayList;

public class CompilatorActivity extends AppCompatActivity {

    //<---------------- Views ---------------->
    //ListView that contains information about registers such as name, old bits and new bits
    private ListView registers;
    private ConstraintLayout cl;
    private MultiAutoCompleteTextView compilatorEditText;
    private ImageButton debugButton, compileButton, runButton, loadFileButton;

    //<---------------- Other Stuff ---------------->
    private Compilator compilator;
    private ArrayList<CustomObjectRegisters> objects;
    private CustomAdapterRegisters adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compilator);

        //<---------------- Views Initializing ---------------->
        registers = (ListView) findViewById(R.id.registersListView);
        cl = (ConstraintLayout) findViewById(R.id.clCompilator);
        compilatorEditText = (MultiAutoCompleteTextView) findViewById(R.id.compilationEditText);

        debugButton = (ImageButton)findViewById(R.id.debugButton);
        compileButton = (ImageButton)findViewById(R.id.compileButton);
        runButton = (ImageButton)findViewById(R.id.runButton);
        loadFileButton = (ImageButton)findViewById(R.id.loadFileButton);

        //<---------------- Other Initializing ---------------->
        objects = new ArrayList<CustomObjectRegisters>();
        adapter = new CustomAdapterRegisters(this, objects, registers);
        compilator = new Compilator(this);
        setHints();
        setOnClickMethods();
    }

    //<---------------- Initializers ---------------->

    //putting few words that will show up when user is trying to write command (writing helper)
    private void setHints() {
        String[] hints = {"SUB", "ADD", "MOV",
                "AX,", "BX,", "CX,", "DX,"};

        compilatorEditText.setTokenizer(new SpaceTokenizer());
        ArrayAdapter<String> adp = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, hints);
        compilatorEditText.setThreshold(0);
        compilatorEditText.setAdapter(adp);
    }

    //determining action for buttons
    private void setOnClickMethods(){

        debugButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animateButtonsClick(debugButton);
            }
        });
        compileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animateButtonsClick(compileButton);
            }
        });
        runButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animateButtonsClick(runButton);
                compilator.analyzeCommand("ADD AX 20");
            }
        });
        loadFileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animateButtonsClick(loadFileButton);

            }
        });
    }



    //adding animation to the view passed as parameter
    private void animateButtonsClick(View v) {
        Animation btnAnim = AnimationUtils.loadAnimation(this, R.anim.button_click_animation);
        btnAnim.setFillAfter(true);
        v.startAnimation(btnAnim);
        v.invalidate();
    }



}
